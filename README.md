# ans_maxiv_role_conda

Ansible role to install `conda` on CentOS and Debian.
`mamba` is also installed by default.

This role includes as well:

- a `conda` module that can be used to manage conda packages.
- a `conda_env` module that can be used to manage conda environments.

## Role Variables

See [defaults/main.yml](defaults/main.yml)

By default the role will only install conda/mamba.
It includes tasks to create conda environments by passing variables but those tasks need to be explicitly run using:

```yaml
  tasks:
    - name: create conda envs
      include_role:
        name: ans_maxiv_role_conda
        tasks_from: create_conda_envs
```

Those tasks will create conda environments if you pass a list of yaml environment files via
the `conda_env_files` variable.
You can use both local files and http/https url:

```yaml
conda_env_files:
  - "{{ playbook_dir }}/tests/python36_env.yml"
  - https://gitlab.maxiv.lu.se/kits-maxiv/ansible-galaxy/ans_maxiv_role_conda/raw/master/molecule/default/tests/python39_env.yml
```

A specific environment can be activated by default using the `conda_default_env` variable.

Those tasks can also be used to deploy cli in conda environments using the `conda_envs` variable.
For example, to deploy `silx`, you can define:

```yaml
conda_envs:
  - env_name: silx
    dependencies:
      - python=3.9
      - silx=0.15.1
    wrappers:
      - silx
    desktop-menus:
      - name: Silx app
        exec: silx
```

`dependencies` can be passed as a list or a dict.
When using a dict, `default` can be used to refer to the version defined in the `versions` dict.

```yaml
conda_envs:
  - env_name: silx
    dependencies:
      python: default
      silx: 0.15.1
    wrappers:
      - silx
    desktop-menus:
      - name: Silx app
        exec: silx
```

`wrappers` and `desktop-menus` are optional.
`desktop-menus` also can take `filename` (default to `exec`), `comment` (default to `name`) and `categories` (default to `MAXIV;`).
`filename` corresponds to the desktop menu file. It should be used if the same command is used with different arguments:

```yaml
conda_envs:
  - env_name: mygui
    dependencies:
      taurusgui-finestcounter: 0.1.0
    wrappers:
      - finestcounter
    desktop-menus:
      - name: Counter for GAS
        filename: finestcounterA
        exec: finestcounter b112a/ctrl/ni6602
      - name: Counter for LUMI
        filename: finestcounterB
        exec: finestcounter b112a/ctrl/ni6602-02
```

The following optional keys are also supported:

- `state`: default to present
- `use_mamba`: default to true
- `channels`: to pass extra channels
- `pip_dependencies`: to install packages with pip in the conda environment
- `pip_index`: to change the default pip index url

Here is how to pass an extra channel:

```yaml
conda_envs:
  - env_name: silx
    channels:
      - maxiv-kits-dev
    dependencies:
      python: 3.9
      silx: 0.15.1
    wrappers:
      - silx
```

If `channels` isn't specified or set to an empty list, the `conda_dev_channel` will be automatically added if a **dev** version is found in the list of dependencies.

To deploy some packages with pip (note that using conda packages is recommended when available):

```yaml
conda_envs:
  - env_name: httpie
    dependencies:
      python: 3.9
    pip_dependencies:
      httpie: 3.2.1
    wrappers:
      - http
      - https
      - httpie
```

If `pip_index` isn't specified or set to an empty string, the `conda_dev_pip_index` will be automatically used if a **dev** version is found in the list of pip dependencies.

The `conda_envs_filter` variable can be used to deploy/update only a specific environment.

## conda module

The `conda` module allows to install, update or remove conda packages.
`mamba` is used by default. Set `use_mamba: false` to use `conda` instead.

```yaml
- name: install flask 1.0 and Python 3.7
  conda:
    name:
      - python=3.7
      - flask=1.0
    state: present
    environment: myapp

- name: install flask from conda-forge
  conda:
    name: flask
    state: present
    environment: flaskapp
    channels:
      - conda-forge

- name: update flask to the latest version
  conda:
    name: flask
    state: latest
    environment: myapp

- name: update conda to the latest version
  conda:
    name: conda
    state: latest

- name: remove flask from myapp environment
  conda:
    name: flask
    state: absent
    environment: myapp
```

## conda_env module

The `conda_env` module allows to create, update or remove conda environments.
Using `mamba` with this module isn't supported yet.

```yaml
- name: create myenv environment
  conda_env:
    name: myenv
    state: present
    file: /tmp/environment.yml

- name: update all packages in myenv environment
  conda_env:
    name: myenv
    state: latest

- name: update myenv environment based on environment.yml
  conda_env:
    name: myenv
    state: present
    prune: true
    file: /tmp/environment.yml

- name: force the environment creation (remove previous one first)
  conda_env:
    state: present
    force: true
    file: /tmp/environment.yml

- name: remove myenv environment
  conda_env:
    name: myenv
    state: absent

- name: create the example-env environment from url
  conda_env:
    name: example-env
    state: present
    file: https://raw.githubusercontent.com/binder-examples/conda/master/environment.yml
```

The `file` given to the `conda_env` module should be a file present locally on the host or a URL.
Passing a URL requires conda >= 4.10.0.

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ans_maxiv_role_conda
```

## License

GPL-3.0-or-later
