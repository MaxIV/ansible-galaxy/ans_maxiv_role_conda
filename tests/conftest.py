import pytest
import yaml


VERSIONS = """
versions:
  python: 3.9
  foo: 1.2.1
  bar: 3.0.0
  package: 3.0.1.dev7+g59aa752
  tango-test: 3.4
"""


@pytest.fixture
def fake_versions(scope="session"):
    return yaml.safe_load(VERSIONS)["versions"]
