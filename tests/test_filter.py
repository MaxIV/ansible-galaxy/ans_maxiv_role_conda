import sys
import pytest

sys.path.insert(1, "filter_plugins")
import conda_filter


@pytest.mark.parametrize(
    "deps, expected",
    [
        (
            [],
            [],
        ),
        (
            ["python=3.10", "foo=1.0.0", "bar"],
            ["python=3.10", "foo=1.0.0", "bar"],
        ),
        (
            {"python": 3.9, "foo": "1.0.0", "bar": "default"},
            ["python=3.9", "foo=1.0.0", "bar=3.0.0"],
        ),
        (
            {"python": "default", "tango-test": "default"},
            ["python=3.9", "tango-test=3.4"],
        ),
        (
            {"python": "default", "tango-test": "latest"},
            ["python=3.9", "tango-test"],
        ),
    ],
)
def test_conda_packages(fake_versions, deps, expected):
    dependencies = conda_filter.conda_packages(deps, fake_versions)
    assert dependencies == expected


@pytest.mark.parametrize(
    "deps, expected",
    [
        (
            [],
            [],
        ),
        (
            ["python=3.10", "foo=1.0.0", "bar"],
            [],
        ),
        (
            ["python=3.10", "foo=latest"],
            [],
        ),
        (
            {"python": 3.9, "foo": "1.0.0", "bar": "default"},
            [],
        ),
        (
            {"python": "default", "tango-test": "latest"},
            ["tango-test"],
        ),
        (
            {"python": "default", "tango-test": "latest", "foo": "latest"},
            ["tango-test", "foo"],
        ),
    ],
)
def test_conda_packages_latest(deps, expected):
    dependencies = conda_filter.conda_packages_latest(deps)
    assert dependencies == expected


@pytest.mark.parametrize(
    "deps, expected",
    [
        (
            [],
            [],
        ),
        (
            ["foo==1.0.0", "bar"],
            ["foo==1.0.0", "bar"],
        ),
        (
            {"foo": "1.0.0", "bar": "default"},
            ["foo==1.0.0", "bar==3.0.0"],
        ),
        (
            {"foo": "latest"},
            ["foo"],
        ),
    ],
)
def test_pip_packages(fake_versions, deps, expected):
    dependencies = conda_filter.pip_packages(deps, fake_versions)
    assert dependencies == expected


@pytest.mark.parametrize(
    "conda_env, expected",
    [
        (
            {"env_name": "myenv", "dependencies": {"foo": "1.0.0"}},
            {"name": "myenv", "dependencies": ["foo=1.0.0"]},
        ),
        (
            {
                "env_name": "myenv",
                "channels": ["extra-channel"],
                "dependencies": {"foo": "1.0.0"},
            },
            {
                "name": "myenv",
                "channels": ["extra-channel"],
                "dependencies": ["foo=1.0.0"],
            },
        ),
        (
            {
                "env_name": "myenv",
                "dependencies": {"foo": "default"},
                "pip_dependencies": {},
            },
            {"name": "myenv", "dependencies": ["foo=1.2.1"]},
        ),
        (
            {
                "env_name": "myenv",
                "dependencies": {"foo": "default"},
                "pip_dependencies": {"bar": "1.0.0"},
            },
            {
                "name": "myenv",
                "dependencies": ["foo=1.2.1", "pip", {"pip": ["bar==1.0.0"]}],
            },
        ),
        (
            {
                "env_name": "myenv",
                "dependencies": {"foo": "default"},
                "pip_dependencies": {"bar": "default"},
            },
            {
                "name": "myenv",
                "dependencies": ["foo=1.2.1", "pip", {"pip": ["bar==3.0.0"]}],
            },
        ),
        (
            {
                "env_name": "myenv",
                "dependencies": {"python": "default"},
                "pip_index": "https://pypi.org/simple",
                "pip_dependencies": {"bar": "default"},
            },
            {
                "name": "myenv",
                "dependencies": [
                    "python=3.9",
                    "pip",
                    {"pip": ["-i https://pypi.org/simple", "bar==3.0.0"]},
                ],
            },
        ),
        (
            {"env_name": "myenv", "dependencies": {"foo": "1.0.0.dev1+g2345111"}},
            {
                "name": "myenv",
                "channels": ["my-dev-channel"],
                "dependencies": ["foo=1.0.0.dev1+g2345111"],
            },
        ),
        (
            {"env_name": "myenv", "dependencies": {"package": "default"}},
            {
                "name": "myenv",
                "channels": ["my-dev-channel"],
                "dependencies": ["package=3.0.1.dev7+g59aa752"],
            },
        ),
        (
            {
                "env_name": "myenv",
                "dependencies": {"package": "default"},
                "channels": [],
            },
            {
                "name": "myenv",
                "channels": ["my-dev-channel"],
                "dependencies": ["package=3.0.1.dev7+g59aa752"],
            },
        ),
        (
            {
                "env_name": "myenv",
                "dependencies": {"package": "default"},
                "channels": ["my-channel"],
            },
            {
                "name": "myenv",
                "channels": ["my-channel"],
                "dependencies": ["package=3.0.1.dev7+g59aa752"],
            },
        ),
        (
            {
                "env_name": "myenv",
                "dependencies": {"python": "default"},
                "pip_dependencies": {"package": "default"},
            },
            {
                "name": "myenv",
                "dependencies": [
                    "python=3.9",
                    "pip",
                    {
                        "pip": [
                            "-i https://my.pypi.org/simple",
                            "package==3.0.1.dev7+g59aa752",
                        ]
                    },
                ],
            },
        ),
    ],
)
def test_conda_env_file(fake_versions, conda_env, expected):
    env_def = conda_filter.conda_env_file(
        conda_env, fake_versions, "my-dev-channel", "https://my.pypi.org/simple"
    )
    assert env_def == expected


@pytest.mark.parametrize(
    "conda_env, expected",
    [
        ({"dependencies": {"foo": "1.0.0"}}, []),
        ({"dependencies": {"foo": "1.0.0"}, "channels": []}, []),
        (
            {"dependencies": {"foo": "1.0.0"}, "channels": ["extra-channel"]},
            ["extra-channel"],
        ),
        ({"dependencies": {"foo": "0.1.2.dev1+g30ba76c"}}, ["my-dev-channel"]),
        (
            {"dependencies": {"foo": "0.1.2.dev1+g30ba76c"}, "channels": []},
            ["my-dev-channel"],
        ),
        (
            {
                "dependencies": {"foo": "0.1.2.dev1+g30ba76c"},
                "channels": ["extra-channel"],
            },
            ["extra-channel"],
        ),
    ],
)
def test_conda_filter_channels(fake_versions, conda_env, expected):
    result = conda_filter.conda_filter_channels(
        conda_env, fake_versions, "my-dev-channel"
    )
    assert result == expected
