def convert_packages(dependencies, versions, separator="="):
    """Convert the passed dependencies to a list of packages with version.

    Accept both a list and dict as input.
    If a dict is passed, "default" can be used to refer to version defined in the "versions" dict.
    """
    if isinstance(dependencies, list):
        return dependencies
    deps = []
    for name, version in dependencies.items():
        if version == "latest":
            package = name
        elif version == "default":
            package = name + separator + str(versions[name])
        else:
            package = name + separator + str(version)
        deps.append(package)
    return deps


def conda_packages(dependencies, versions):
    """Filter to accept both list and dict as conda packages"""
    return convert_packages(dependencies, versions, "=")


def pip_packages(dependencies, versions):
    """Filter to accept both list and dict as pip packages"""
    return convert_packages(dependencies, versions, "==")


def conda_packages_latest(dependencies):
    """Filter to return the packages to be updated to latest

    Return an empty list if a list is passed instead of dict (for backward compatibility)
    """
    if isinstance(dependencies, list):
        return []
    return [name for name, version in dependencies.items() if version == "latest"]


def conda_filter_channels(conda_env, versions, dev_channel):
    """Return the channels to pass

    Return the given channels option if not empty.
    Otherwise, the dev_channel is automatically added if a dev version is found.
    """
    if "channels" in conda_env and conda_env["channels"]:
        return conda_env["channels"]
    if is_dev_package(conda_packages(conda_env["dependencies"], versions), "="):
        return [dev_channel]
    return []


def is_dev_package(packages, separator):
    """Return True if there is a dev package in the list"""
    for pkg in packages:
        # In case a list is passed as dependencies instead of a dict, there might be no version
        # Same if "latest" is used
        try:
            _, version = pkg.split(separator)
        except ValueError:
            continue
        if "dev" in version:
            return True
    return False


def conda_env_file(conda_env, versions, dev_channel, dev_pip_index):
    env_def = {"name": conda_env["env_name"]}
    channels = conda_filter_channels(conda_env, versions, dev_channel)
    if channels:
        env_def["channels"] = channels
    env_def["dependencies"] = conda_packages(conda_env["dependencies"], versions)
    if conda_env.get("pip_dependencies", []):
        env_def["dependencies"].append("pip")
        pip_dependencies = pip_packages(conda_env["pip_dependencies"], versions)
        if "pip_index" in conda_env and conda_env["pip_index"]:
            pip_dependencies.insert(0, "-i {0}".format(conda_env["pip_index"]))
        elif is_dev_package(pip_dependencies, "=="):
            pip_dependencies.insert(0, "-i {0}".format(dev_pip_index))
        env_def["dependencies"].append({"pip": pip_dependencies})
    return env_def


class FilterModule(object):
    """Ansible conda jinja2 filters"""

    def filters(self):
        filters = {
            "conda_packages": conda_packages,
            "pip_packages": pip_packages,
            "conda_packages_latest": conda_packages_latest,
            "conda_env_file": conda_env_file,
            "conda_filter_channels": conda_filter_channels,
        }
        return filters
