---
- name: make sure conda environments don't exist
  file:
    path: "{{ conda_path }}/envs/{{ item }}"
    state: absent
  loop:
    - myenv
    - myenv1
    - myenv2
    - myenv-url

- name: copy environment.yml files
  copy:
    src: "{{ item }}"
    dest: "/tmp/{{ item }}"
  loop:
    - environment_python381.yml
    - environment_python386.yml

- name: create myenv environment
  conda_env:
    name: myenv
    file: /tmp/environment_python381.yml
  register: myenv_creation

- name: verify we recorded a change
  assert:
    that:
      - myenv_creation.changed

- name: check Python version in the created environment
  command: "{{ conda_path }}/envs/myenv/bin/python --version"
  register: python_version

- name: verify Python 3.8.1 was installed
  assert:
    that:
      - "'Python 3.8.1' in python_version.stdout"

- name: update myenv with the same environment.yml file
  conda_env:
    name: myenv
    file: /tmp/environment_python381.yml
  register: myenv_update1

- name: verify we didn't record a change
  assert:
    that:
      - not myenv_update1.changed

- name: force myenv creation with the same environment.yml file
  conda_env:
    name: myenv
    force: true
    file: /tmp/environment_python381.yml
  register: myenv_force1

- name: verify we recorded a change
  assert:
    that:
      - myenv_force1.changed

- name: update myenv with a new environment.yml file
  conda_env:
    name: myenv
    file: /tmp/environment_python386.yml
  register: myenv_update2

- name: verify we recorded a change
  assert:
    that:
      - myenv_update2.changed

- name: check Python version in the updated environment
  command: "{{ conda_path }}/envs/myenv/bin/python --version"
  register: python_version

- name: verify Python 3.8.6 was installed
  assert:
    that:
      - "'Python 3.8.6' in python_version.stdout"

- name: delete myenv
  conda_env:
    name: myenv
    state: absent
  register: delete_myenv

- name: verify we recorded a change
  assert:
    that:
      - delete_myenv.changed

- name: check if myenv exists
  stat:
    path: "{{ conda_path }}/envs/myenv"
  register: myenv_stat

- name: verify that myenv was removed
  assert:
    that:
      - not myenv_stat.stat.exists

- name: delete myenv again
  conda_env:
    name: myenv
    state: absent
  register: delete_myenv2

- name: verify we didn't record a change
  assert:
    that:
      - not delete_myenv2.changed

- name: create myenv1 environment without name
  conda_env:
    file: /tmp/environment_python381.yml
  register: myenv1_creation

- name: verify we recorded a change
  assert:
    that:
      - myenv1_creation.changed

- name: update all myenv1 packages
  conda_env:
    name: myenv1
    state: latest
  register: myenv1_update1

- name: verify we recorded a change
  assert:
    that:
      - myenv1_update1.changed

- name: update all myenv1 packages again
  conda_env:
    name: myenv1
    state: latest
  register: myenv1_update2

- name: verify we didn't record a change
  assert:
    that:
      - not myenv1_update2.changed

- name: create myenv-url environment from url
  conda_env:
    name: myenv-url
    file: https://gitlab.maxiv.lu.se/kits-maxiv/ansible-galaxy/ans_maxiv_role_conda/raw/master/molecule/default/tests/python39_env.yml
  register: myenv_url_creation

- name: verify we recorded a change
  assert:
    that:
      - myenv_url_creation.changed

- name: check Python version in the created environment
  command: "{{ conda_path }}/envs/myenv-url/bin/python --version"
  register: python_version

- name: verify Python 3.9 was installed
  assert:
    that:
      - "'Python 3.9' in python_version.stdout"

- name: update myenv-url with the same environment.yml file
  conda_env:
    name: myenv-url
    file: https://gitlab.maxiv.lu.se/kits-maxiv/ansible-galaxy/ans_maxiv_role_conda/raw/master/molecule/default/tests/python39_env.yml
  register: myenv_url_update

- name: verify we didn't record a change
  assert:
    that:
      - not myenv_url_update.changed
