import os
import pytest
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


@pytest.fixture(scope="module")
def inventory_hostname(host):
    return host.ansible.get_variables()["inventory_hostname"]


def test_conda_mamba_version(host):
    cmd = host.run("/opt/conda/bin/mamba --version 2>&1")
    assert cmd.rc == 0
    assert "conda 4.14.0" in cmd.stdout.strip()
    assert "mamba 0.25.0" in cmd.stdout.strip()


@pytest.mark.parametrize("name", ["conda", "mamba"])
def test_conda_mamba_activation(host, name):
    cmd = host.run(f'su -l -c "type {name}" root')
    assert cmd.stdout.startswith(f"{name} is a function")


def test_condarc_file(host, inventory_hostname):
    condarc = host.file("/opt/conda/.condarc")
    if inventory_hostname == "conda-debian":
        assert condarc.content_string.splitlines() == [
            "auto_activate_base: true",
            "auto_update_conda: false",
        ]
    else:
        assert condarc.content_string.splitlines() == [
            "auto_activate_base: false",
            "auto_update_conda: false",
            "channels:",
            "- conda-forge",
        ]


def test_conda_created_env(host, inventory_hostname):
    conda_envs = host.file("/opt/conda/envs").listdir()
    if inventory_hostname == "conda-env-file-centos7":
        assert sorted(conda_envs) == ["python36", "python39"]
        cmd = host.run("/opt/conda/envs/python36/bin/python --version 2>&1")
        assert cmd.stdout.startswith("Python 3.6")
        cmd = host.run("/opt/conda/envs/python39/bin/python --version 2>&1")
        assert cmd.stdout.startswith("Python 3.9")
    elif inventory_hostname == "conda-env-rocky8":
        assert sorted(conda_envs) == ["curl", "httpie", "optional", "test"]
        # check channels used
        cmd = host.run("/opt/conda/bin/conda env export -n curl")
        assert "- conda-forge" in cmd.stdout
        assert "- bioconda" not in cmd.stdout
        cmd = host.run("/opt/conda/bin/conda env export -n optional")
        assert "- conda-forge" in cmd.stdout
        assert "- bioconda" in cmd.stdout
    else:
        assert conda_envs == []


def test_conda_default_env_file(host, inventory_hostname):
    conda_default_env = host.file("/etc/profile.d/conda_default_env.sh")
    if inventory_hostname == "conda-env-file-centos7":
        assert conda_default_env.content_string.splitlines() == [
            r"export PS1='[\u@\h \W]\$ '",
            "conda activate python39",
        ]
    else:
        assert not conda_default_env.exists


def test_conda_default_env_activation(host, inventory_hostname):
    cmd = host.run('su -l -c "python --version 2>&1" root')
    if inventory_hostname == "conda-env-file-centos7":
        # python39 env activated
        assert cmd.stdout.startswith("Python 3.9")
    # We don't test Debian here because only python3 is installed in the image
    # It could be tested separately but that wouldn't bring much value
    elif inventory_hostname == "conda-centos7":
        # no env activated
        assert cmd.stdout.startswith("Python 2.7")


@pytest.mark.parametrize(
    "env_name, cli",
    [
        ("test", "python"),
        ("test", "wget"),
        ("curl", "curl"),
        ("httpie", "http"),
        ("httpie", "httpie"),
    ],
)
def test_conda_cli(host, inventory_hostname, env_name, cli):
    cmd = host.run(f"/usr/local/bin/{cli} --version")
    if inventory_hostname == "conda-env-rocky8":
        assert cmd.rc == 0
        assert host.file(f"/usr/local/bin/{cli}").contains(f"conda activate {env_name}")
    else:
        # command not found
        assert cmd.rc == 127


def test_conda_desktop_menu(host, inventory_hostname):
    wget_desktop = host.file("/usr/share/applications/wget.desktop")
    wgetB_desktop = host.file("/usr/share/applications/wgetB.desktop")
    wget3_desktop = host.file("/usr/share/applications/wget--verbose.desktop")
    curl_desktop = host.file("/usr/share/applications/curl.desktop")
    if inventory_hostname == "conda-env-rocky8":
        assert wget_desktop.exists
        assert wget_desktop.contains("Name=My wget")
        assert wget_desktop.contains("Comment=My wget")
        assert wget_desktop.contains("Exec=/usr/local/bin/wget")
        assert wget_desktop.contains("Categories=MAXIV;")
        assert wgetB_desktop.exists
        assert wgetB_desktop.contains("Name=Second wget")
        assert wgetB_desktop.contains("Exec=/usr/local/bin/wget --verbose")
        assert wget3_desktop.exists
        assert wget3_desktop.contains("Exec=/usr/local/bin/wget --verbose")
        assert curl_desktop.exists
        assert curl_desktop.contains("Name=My curl")
        assert curl_desktop.contains("Comment=My curl app")
        assert curl_desktop.contains("Exec=/usr/local/bin/curl")
        assert curl_desktop.contains("Categories=My category;Foo;")
    else:
        assert not wget_desktop.exists
        assert not wgetB_desktop.exists
        assert not wget3_desktop.exists
        assert not curl_desktop.exists
