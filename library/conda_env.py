#!/usr/bin/python

# Copyright: (c) 2019, Benjamin Bertrand <beenje@gmail.com>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

from __future__ import absolute_import, division, print_function

__metaclass__ = type

ANSIBLE_METADATA = {
    "metadata_version": "1.1",
    "status": ["preview"],
    "supported_by": "community",
}

DOCUMENTATION = """
---
module: conda_env

short_description: Manages I(conda) environment

version_added: "2.9"

description:
    - Create, update, remove I(conda) environments.
    - This module requires conda to be already installed.
    - The minimum conda version required is 4.7.12.

options:
    name:
        description:
            - The name or full path of the environment. Override the name in the environment.yml file if defined.
        required: false
        type: str
    state:
        description:
            - Wether to create (C(present)), update (C(latest)) or remove (C(absent)) the environment
            - C(present) will ensure that the given environment exits and matches the environment.yml file.
            - C(latest) will update all packages in the environment. It is exclusive with file.
            - C(absent) will remove the specified environment.
            - If the environment doesn't exist, it will be created.
        choices: [ absent, latest, present ]
        default: present
        type: str
    file:
        description:
            - The path to a conda environment.yml file, which should be local to the remote system.
        type: path
    executable:
        description:
            - Full path of the conda command to use, like C(/home/conda/bin/conda).
            - If not specified, C(conda) will be searched in the PATH as well as
              the /opt/conda/bin directory.
        type: path
    prune:
        description:
            - Remove installed packages not defined in environment.yml. Can only be used with state c(present).
        type: bool
    force:
        description:
            - Force the creation of an environment by first removing a previously existing environment of the same name.
              Can only be used with state c(present).
        type: bool

requirements:
  - conda >= 4.7.12

author:
    - Benjamin Bertrand (@beenje)
"""

EXAMPLES = """
- name: create myenv environment
  conda_env:
    name: myenv
    state: present
    file: /tmp/environment.yml

- name: update all packages in myenv environment
  conda_env:
    name: myenv
    state: latest

- name: update myenv environment based on environment.yml
  conda_env:
    name: myenv
    state: present
    prune: true
    file: /tmp/environment.yml

- name: force the environment creation (remove previous one first)
  conda_env:
    state: present
    force: true
    file: /tmp/environment.yml

- name: remove myenv environment
  conda_env:
    name: myenv
    state: absent
"""

RETURN = """
cmd:
    description: The conda command that was run
    type: list
    returned: always
rc:
    description: The return code of the command
    type: int
    returned: always
stdout_json:
    description: The json output of the command
    type: dict
    returned: always
stderr:
    description: The standard error of the command
    type: str
    returned: always
"""

import json
import os
from collections import namedtuple
from ansible.module_utils.basic import AnsibleModule

Result = namedtuple("Result", "changed cmd rc stdout_json stderr")


class Conda:
    """Class to perform conda operations"""

    def __init__(
        self,
        module,
        environment,
        executable=None,
        use_mamba=False,
        channels=None,
        check_mode=False,
    ):
        self.module = module
        if use_mamba:
            exe = "mamba"
        else:
            exe = "conda"
        self.executable = executable or module.get_bin_path(
            exe, required=True, opt_dirs=["/opt/conda/bin"]
        )
        if environment is None:
            self.env_args = []
        else:
            if os.path.sep in environment:
                env_flag = "--prefix"
            else:
                env_flag = "--name"
            self.env_args = [env_flag, environment]
        self.default_args = ["-y"] + self.env_args
        if channels:
            for channel in channels:
                self.default_args.extend(["--channel", channel])
        if check_mode:
            self.default_args.append("--dry-run")

    @staticmethod
    def changed(stdout_json):
        """Return True if any change was performed by the conda command"""
        # When conda didn't install/update anything, the output is:
        # {
        #  "message": "All requested packages already installed.",
        #  "success": true
        # }
        # When conda has some operations to perform, the list of actions
        # is returned in the json output:
        # {
        #  "actions": {
        #   "FETCH": [],
        #   "LINK": [
        #    {
        #     "base_url": "https://repo.anaconda.com/pkgs/main",
        #     "build_number": 0,
        #     "build_string": "py_0",
        #     "channel": "pkgs/main",
        #     "dist_name": "flask-1.1.1-py_0",
        #     "name": "flask",
        #     "platform": "noarch",
        #     "version": "1.1.1"
        #    }
        #   ],
        #   "PREFIX": "/opt/conda/envs/python3"
        #  },
        #  "prefix": "/opt/conda/envs/python3",
        #  "success": true
        # }
        if "actions" not in stdout_json:
            return False
        return True

    def run_conda(self, cmd, *args, **kwargs):
        """Run a conda commmand"""
        fail_on_error = kwargs.pop("fail_on_error", True)
        subcmd = kwargs.pop("subcmd", None)
        if subcmd is None:
            command = [self.executable]
        else:
            command = [self.executable, subcmd]
        command.extend([cmd, "--quiet", "--json"])
        command.extend(args)
        rc, stdout, stderr = self.module.run_command(command)
        if fail_on_error and rc != 0:
            self.module.fail_json(
                command=command,
                msg="Command failed",
                rc=rc,
                stdout=stdout,
                stderr=stderr,
            )
        try:
            stdout_json = json.loads(stdout)
        except ValueError:
            if cmd == "remove":
                # conda remove --all doesn't output anything on stdout if the
                # environment doesn't exist (in 4.7.12)
                if stdout.strip() == "":
                    return Result(False, command, rc, {}, stderr)
            self.module.fail_json(
                command=command,
                msg="Failed to parse the output of the command",
                stdout=stdout,
                stderr=stderr,
            )
        return Result(self.changed(stdout_json), command, rc, stdout_json, stderr)

    def list_packages(self):
        """Return the list of packages name installed in the environment"""
        result = self.run_conda("list", *self.env_args)
        return [pkg["name"] for pkg in result.stdout_json]

    def env_exists(self):
        """Return True if the environment exists

        The existence is checked by running the conda list -n/-p environment command.
        """
        result = self.run_conda("list", *self.env_args, fail_on_error=False)
        return result.rc == 0

    def install(self, packages):
        """Install the given conda packages"""
        args = self.default_args + packages
        return self.run_conda("install", "-S", *args)

    def update(self, packages):
        """Update the given conda packages"""
        args = self.default_args + packages
        return self.run_conda("update", *args)

    def update_all(self):
        """Update all packages in the environment"""
        return self.run_conda("update", "--all", *self.env_args)

    def create(self, packages):
        """Create a new environment with the given conda packages"""
        args = self.default_args + packages
        return self.run_conda("create", *args)

    def remove(self, packages):
        """Remove the conda packages from the environment"""
        installed_packages = self.list_packages()
        # clean the packages name by removing the version spec
        # to keep only the package name in lowercase
        packages_names = [pkg.split("=")[0].lower() for pkg in packages]
        packages_to_remove = set(installed_packages) & set(packages_names)
        if packages_to_remove:
            args = self.default_args + list(packages_to_remove)
            return self.run_conda("remove", *args)
        else:
            # None of the given packages are in the environment
            # Nothing to do
            return Result(False, "", 0, {}, "")

    def env_update(self, file, prune=False):
        """Create or update the environment based on file"""
        args = ["-f", file] + self.env_args
        if prune:
            args.append("--prune")
        return self.run_conda("update", subcmd="env", *args)

    def env_create(self, file, force=False):
        """Create the environment based on file"""
        args = ["-f", file] + self.env_args
        if force:
            args.append("--force")
        return self.run_conda("create", subcmd="env", *args)

    def env_remove(self):
        """Remove the given environment"""
        return self.run_conda("remove", "--all", *self.env_args)


def run_module():
    module_args = dict(
        name=dict(type="str"),
        state=dict(choices=["present", "absent", "latest"], default="present"),
        executable=dict(type="path"),
        file=dict(type="path"),
        prune=dict(type="bool", default=False),
        force=dict(type="bool", default=False),
    )

    module = AnsibleModule(
        argument_spec=module_args,
        required_one_of=[["name", "file"]],
        required_if=([("state", "absent", ["name"]), ("state", "latest", ["name"])]),
        supports_check_mode=False,
    )
    state = module.params["state"]

    conda = Conda(
        module,
        environment=module.params["name"],
        executable=module.params["executable"],
    )

    if state == "present":
        if module.params["force"]:
            result = conda.env_create(module.params["file"], module.params["force"])
        else:
            result = conda.env_update(module.params["file"], module.params["prune"])
    elif state == "latest":
        result = conda.update_all()
    elif state == "absent":
        result = conda.env_remove()

    module.exit_json(**result._asdict())


def main():
    run_module()


if __name__ == "__main__":
    main()
